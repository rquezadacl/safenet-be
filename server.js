var parseurl = require('parseurl');
var md5 = require('md5');
var SECRET_KEY="Paglaho";
var crypto = require('crypto'),
    algorithm = 'aes192';
var fs = require('fs');
var cors = require('cors');
var session = require('express-session');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var http = require('http');
var https = require('https');
const jwt  = require('jsonwebtoken');
var fileUpload = require('express-fileupload');
var helper = require('./app/models/helper');
var Usuarios = require('./app/models/usuariosModel.js');
const express = require('express'),
  app = express(),
  bodyParser = require('body-parser');
  port = process.env.PORT || 8400;
  portssl=8444;
  app.use(cors());
  app.use(fileUpload());
  app.use(session({
		secret: 'keyboard cat',
		resave: false,
		saveUninitialized: true
	}));
app.use(function (req, res, next) {
    res.header('Content-Type', 'application/json');
    next();
});

const mysql = require('mysql');
// connection configurations
const mc = mysql.createConnection({
    host     : 'ls-21e3be483b4cb1293f403e1b9419d648662a00c9.cjeohbjiijkm.ca-central-1.rds.amazonaws.com',
    port     : '3306',
    user     : 'novadb',
    password : '_6Xq5X#^3?QoF[;9EUgS7pLrz;s&}Ak+',
    database : 'safe_net'

});
 
// connect to database
mc.connect();
//uncomment options for ssl
//var options = {
//    key: fs.readFileSync('/domain.com.key'),
//    cert: fs.readFileSync('/certificate.crt')
//};
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Credentials', false);
    res.header('Access-Control-Max-Age', '86400');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, x_chord, y_chord, z_chord, d_chord');
    next();
};

var encrypt = function(text){
    var cipher = crypto.createCipher(algorithm,SECRET_KEY);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
};

var decrypt = function(text){
    var decipher = crypto.createDecipher(algorithm,SECRET_KEY);
    var dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;
};

const jwtKey = 'my_secret_key';
const jwtExpirySeconds ="10h";

const users = {
  user1: 'password1',
  user2: 'password2'
}

const tokenGen = (req, res) => {
  const secret = '31082004-19032009-NC';
 
  // Get credentials from JSON body
  const { username, password } = req.body
  
  var hashpassword = crypto.createHmac('sha512', '31082004-19032009-NC').update(password).digest('hex');

  Usuarios.login(username,function(err, usuarios) {
    if(err) {
      return res.status(401).end()
    } else if(usuarios=== null){
      return res.status(401).end()
    } else {
      if (!username || !password) {
        // return 401 error is username or password doesn't exist, or if password does
        // not match the password in our records
        return res.status(401).end()
      } else if(usuarios[0].username === username && usuarios[0].password === hashpassword){
          // Create a new token with the username in the payload
          // and which expires 1 day after issue
          //jwtExpirySeconds ="20d" // it will be expired after 20 days
          //jwtExpirySeconds= 120 // it will be expired after 120ms
          var datajwt = {
            userid: usuarios[0].idusuario,
            name: usuarios[0].nombre,
            lastname: usuarios[0].apellido,
            email: usuarios[0].email,
            idrol: usuarios[0].idrol,
            idestado: usuarios[0].idestado,
            activado: usuarios[0].activado,
            idcliente: usuarios[0].idcliente
          };

          const token = jwt.sign({ result: datajwt }, jwtKey, {
            algorithm: 'HS256',
            expiresIn: jwtExpirySeconds
          })
          console.log('token:', token)

          var response = {"expires_in":jwtExpirySeconds,"access_token":token,"token_type": "bearer"};
          res.status(200).send(helper.createResponse(helper.Success,1,"Token Generated",response));
      } else {
        var response = {"error":"usuario o contraseña no valido."};
          res.status(200).send(helper.createResponse(helper.error,1,"Token not Generated",response));
      }
    }
  });


  
  
}


app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(allowCrossDomain);

http.createServer(app).listen(port);
// Create an HTTPS service identical to the HTTP service.
//https.createServer(options, app).listen(portssl);
//app.listen(port);

console.log('API server started on: ' + port);

app.post('/token', tokenGen);
var busRoutes = require('./app/routes/busRoute');
var cajaRoutes = require('./app/routes/cajaRoute');
var claseRoutes = require('./app/routes/claseRoute');
var clienteRoutes = require('./app/routes/clienteRoute');
var dispositivoRoutes = require('./app/routes/dispositivoRoute');
var estadoRoutes = require('./app/routes/estadoRoute');
var gps_data_devicesRoutes = require('./app/routes/gps_data_devicesRoute');
var lugarRoutes = require('./app/routes/lugarRoute');
var lugar_respaldoRoutes = require('./app/routes/lugar_respaldoRoute');
var menuRoutes = require('./app/routes/menuRoute');
var nivelRoutes = require('./app/routes/nivelRoute');
var paradaRoutes = require('./app/routes/paradaRoute');
var pasajeroRoutes = require('./app/routes/pasajeroRoute');
var precioRoutes = require('./app/routes/precioRoute');
var reporteRoutes = require('./app/routes/reporteRoute');
var rolesRoutes = require('./app/routes/rolesRoute');
var rutaRoutes = require('./app/routes/rutaRoute');
var servicioRoutes = require('./app/routes/servicioRoute');
var testsqlRoutes = require('./app/routes/testsqlRoute');
var tipoRoutes = require('./app/routes/tipoRoute');
var tramoRoutes = require('./app/routes/tramoRoute');
var usuariosRoutes = require('./app/routes/usuariosRoute');

busRoutes(app)
cajaRoutes(app)
claseRoutes(app)
clienteRoutes(app)
dispositivoRoutes(app)
estadoRoutes(app)
gps_data_devicesRoutes(app)
lugarRoutes(app)
lugar_respaldoRoutes(app)
menuRoutes(app)
nivelRoutes(app)
paradaRoutes(app)
pasajeroRoutes(app)
precioRoutes(app)
reporteRoutes(app)
rolesRoutes(app)
rutaRoutes(app)
servicioRoutes(app)
testsqlRoutes(app)
tipoRoutes(app)
tramoRoutes(app)
usuariosRoutes(app)


