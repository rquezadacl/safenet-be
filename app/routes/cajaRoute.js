'use strict';
module.exports = function(app) {
  var cajaInstance = require('../controllers/cajaController');

  app.route('/caja')
    .get(cajaInstance.listAll)
    .post(cajaInstance.createNew);
   
   app.route('/caja/:id')
    .get(cajaInstance.readById)
    .put(cajaInstance.updateById)
    .delete(cajaInstance.deleteById);

	app.route('/caja/search/:searchKey')
    .get(cajaInstance.search);
    };
