'use strict';
module.exports = function(app) {
  var usuariosInstance = require('../controllers/usuariosController');

  app.route('/usuarios')
    .get(usuariosInstance.listAll)
    .post(usuariosInstance.createNew);
   
   app.route('/usuarios/:idusuario')
    .get(usuariosInstance.readById)
    .put(usuariosInstance.updateById)
    .delete(usuariosInstance.deleteById);

	app.route('/usuarios/search/:searchKey')
    .get(usuariosInstance.search);
    };
