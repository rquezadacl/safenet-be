'use strict';
module.exports = function(app) {
  var estadoInstance = require('../controllers/estadoController');

  app.route('/estado')
    .get(estadoInstance.listAll)
    .post(estadoInstance.createNew);
   
   app.route('/estado/:idestado')
    .get(estadoInstance.readById)
    .put(estadoInstance.updateById)
    .delete(estadoInstance.deleteById);

	app.route('/estado/search/:searchKey')
    .get(estadoInstance.search);
    };
