'use strict';
module.exports = function(app) {
  var gps_data_devicesInstance = require('../controllers/gps_data_devicesController');

  app.route('/gps_data_devices')
    .get(gps_data_devicesInstance.listAll)
    .post(gps_data_devicesInstance.createNew);
   
   app.route('/gps_data_devices/:id')
    .get(gps_data_devicesInstance.readById)
    .put(gps_data_devicesInstance.updateById)
    .delete(gps_data_devicesInstance.deleteById);

	app.route('/gps_data_devices/search/:searchKey')
    .get(gps_data_devicesInstance.search);
    };
