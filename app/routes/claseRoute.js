'use strict';
module.exports = function(app) {
  var claseInstance = require('../controllers/claseController');

  app.route('/clase')
    .get(claseInstance.listAll)
    .post(claseInstance.createNew);
   
   app.route('/clase/:idclase')
    .get(claseInstance.readById)
    .put(claseInstance.updateById)
    .delete(claseInstance.deleteById);

	app.route('/clase/search/:searchKey')
    .get(claseInstance.search);
    };
