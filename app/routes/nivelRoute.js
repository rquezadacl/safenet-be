'use strict';
module.exports = function(app) {
  var nivelInstance = require('../controllers/nivelController');

  app.route('/nivel')
    .get(nivelInstance.listAll)
    .post(nivelInstance.createNew);
   
   app.route('/nivel/:idnivel')
    .get(nivelInstance.readById)
    .put(nivelInstance.updateById)
    .delete(nivelInstance.deleteById);

	app.route('/nivel/search/:searchKey')
    .get(nivelInstance.search);
    };
