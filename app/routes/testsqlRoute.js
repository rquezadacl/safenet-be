'use strict';
module.exports = function(app) {
  var testsqlInstance = require('../controllers/testsqlController');

  app.route('/testsql')
    .get(testsqlInstance.listAll)
    .post(testsqlInstance.createNew);
   
   app.route('/testsql/:id')
    .get(testsqlInstance.readById)
    .put(testsqlInstance.updateById)
    .delete(testsqlInstance.deleteById);

	app.route('/testsql/search/:searchKey')
    .get(testsqlInstance.search);
    };
