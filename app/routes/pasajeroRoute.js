'use strict';
module.exports = function(app) {
  var pasajeroInstance = require('../controllers/pasajeroController');

  app.route('/pasajero')
    .get(pasajeroInstance.listAll)
    .post(pasajeroInstance.createNew);
   
   app.route('/pasajero/:id')
    .get(pasajeroInstance.readById)
    .put(pasajeroInstance.updateById)
    .delete(pasajeroInstance.deleteById);

	app.route('/pasajero/search/:searchKey')
    .get(pasajeroInstance.search);
    };
