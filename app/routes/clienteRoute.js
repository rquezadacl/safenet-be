'use strict';
module.exports = function(app) {
  var clienteInstance = require('../controllers/clienteController');

  app.route('/cliente')
    .get(clienteInstance.listAll)
    .post(clienteInstance.createNew);
   
   app.route('/cliente/:id')
    .get(clienteInstance.readById)
    .put(clienteInstance.updateById)
    .delete(clienteInstance.deleteById);

	app.route('/cliente/search/:searchKey')
    .get(clienteInstance.search);
    };
