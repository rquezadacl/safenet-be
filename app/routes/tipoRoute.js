'use strict';
module.exports = function(app) {
  var tipoInstance = require('../controllers/tipoController');

  app.route('/tipo')
    .get(tipoInstance.listAll)
    .post(tipoInstance.createNew);
   
   app.route('/tipo/:idtipo')
    .get(tipoInstance.readById)
    .put(tipoInstance.updateById)
    .delete(tipoInstance.deleteById);

	app.route('/tipo/search/:searchKey')
    .get(tipoInstance.search);
    };
