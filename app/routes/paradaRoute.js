'use strict';
module.exports = function(app) {
  var paradaInstance = require('../controllers/paradaController');

  app.route('/parada')
    .get(paradaInstance.listAll)
    .post(paradaInstance.createNew);
   
   app.route('/parada/:idparada')
    .get(paradaInstance.readById)
    .put(paradaInstance.updateById)
    .delete(paradaInstance.deleteById);

	app.route('/parada/search/:searchKey')
    .get(paradaInstance.search);
    };
