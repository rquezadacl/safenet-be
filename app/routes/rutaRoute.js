'use strict';
module.exports = function(app) {
  var rutaInstance = require('../controllers/rutaController');

  app.route('/ruta')
    .get(rutaInstance.listAll)
    .post(rutaInstance.createNew);
   
   app.route('/ruta/:id')
    .get(rutaInstance.readById)
    .put(rutaInstance.updateById)
    .delete(rutaInstance.deleteById);

	app.route('/ruta/search/:searchKey')
    .get(rutaInstance.search);
    };
