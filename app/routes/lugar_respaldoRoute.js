'use strict';
module.exports = function(app) {
  var lugar_respaldoInstance = require('../controllers/lugar_respaldoController');

  app.route('/lugar_respaldo')
    .get(lugar_respaldoInstance.listAll)
    .post(lugar_respaldoInstance.createNew);
   
   app.route('/lugar_respaldo/:id')
    .get(lugar_respaldoInstance.readById)
    .put(lugar_respaldoInstance.updateById)
    .delete(lugar_respaldoInstance.deleteById);

	app.route('/lugar_respaldo/search/:searchKey')
    .get(lugar_respaldoInstance.search);
    };
