'use strict';
module.exports = function(app) {
  var menuInstance = require('../controllers/menuController');

  app.route('/menu')
    .get(menuInstance.listAll)
    .post(menuInstance.createNew);
   
   app.route('/menu/:idmenu')
    .get(menuInstance.readById)
    .put(menuInstance.updateById)
    .delete(menuInstance.deleteById);

	app.route('/menu/search/:searchKey')
    .get(menuInstance.search);
    };
