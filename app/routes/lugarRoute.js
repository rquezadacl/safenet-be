'use strict';
module.exports = function(app) {
  var lugarInstance = require('../controllers/lugarController');

  app.route('/lugar')
    .get(lugarInstance.listAll)
    .post(lugarInstance.createNew);
   
   app.route('/lugar/:id')
    .get(lugarInstance.readById)
    .put(lugarInstance.updateById)
    .delete(lugarInstance.deleteById);

	app.route('/lugar/search/:searchKey')
    .get(lugarInstance.search);
    };
