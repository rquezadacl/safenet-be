'use strict';
module.exports = function(app) {
  var tramoInstance = require('../controllers/tramoController');

  app.route('/tramo')
    .get(tramoInstance.listAll)
    .post(tramoInstance.createNew);
   
   app.route('/tramo/:id')
    .get(tramoInstance.readById)
    .put(tramoInstance.updateById)
    .delete(tramoInstance.deleteById);

	app.route('/tramo/search/:searchKey')
    .get(tramoInstance.search);
    };
