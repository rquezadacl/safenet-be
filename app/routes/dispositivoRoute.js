'use strict';
module.exports = function(app) {
  var dispositivoInstance = require('../controllers/dispositivoController');

  app.route('/dispositivo')
    .get(dispositivoInstance.listAll)
    .post(dispositivoInstance.createNew);
   
   app.route('/dispositivo/:id')
    .get(dispositivoInstance.readById)
    .put(dispositivoInstance.updateById)
    .delete(dispositivoInstance.deleteById);

	app.route('/dispositivo/search/:searchKey')
    .get(dispositivoInstance.search);
    };
