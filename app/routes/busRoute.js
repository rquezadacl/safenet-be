'use strict';
module.exports = function(app) {
  var busInstance = require('../controllers/busController');

  app.route('/bus')
    .get(busInstance.listAll)
    .post(busInstance.createNew);
   
   app.route('/bus/:id')
    .get(busInstance.readById)
    .put(busInstance.updateById)
    .delete(busInstance.deleteById);

	app.route('/bus/search/:searchKey')
    .get(busInstance.search);
    };
