'use strict';
module.exports = function(app) {
  var servicioInstance = require('../controllers/servicioController');

  app.route('/servicio')
    .get(servicioInstance.listAll)
    .post(servicioInstance.createNew);
   
   app.route('/servicio/:id')
    .get(servicioInstance.readById)
    .put(servicioInstance.updateById)
    .delete(servicioInstance.deleteById);

	app.route('/servicio/search/:searchKey')
    .get(servicioInstance.search);
    };
