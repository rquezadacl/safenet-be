'use strict';
module.exports = function(app) {
  var precioInstance = require('../controllers/precioController');

  app.route('/precio')
    .get(precioInstance.listAll)
    .post(precioInstance.createNew);
   
   app.route('/precio/:id')
    .get(precioInstance.readById)
    .put(precioInstance.updateById)
    .delete(precioInstance.deleteById);

	app.route('/precio/search/:searchKey')
    .get(precioInstance.search);
    };
