'use strict';
module.exports = function(app) {
  var reporteInstance = require('../controllers/reporteController');

  app.route('/reporte')
    .get(reporteInstance.listAll)
    .post(reporteInstance.createNew);
   
   app.route('/reporte/:id')
    .get(reporteInstance.readById)
    .put(reporteInstance.updateById)
    .delete(reporteInstance.deleteById);

	app.route('/reporte/search/:searchKey')
    .get(reporteInstance.search);
    };
