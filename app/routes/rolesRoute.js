'use strict';
module.exports = function(app) {
  var rolesInstance = require('../controllers/rolesController');

  app.route('/roles')
    .get(rolesInstance.listAll)
    .post(rolesInstance.createNew);
   
   app.route('/roles/:idrol')
    .get(rolesInstance.readById)
    .put(rolesInstance.updateById)
    .delete(rolesInstance.deleteById);

	app.route('/roles/search/:searchKey')
    .get(rolesInstance.search);
    };
