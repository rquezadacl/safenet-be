'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Parada object constructor
var Parada = function(req,parada){
    
this.idparada = 0;
this.nombre = parada.nombre;
this.tipo = parada.tipo;
this.idTraccar = parada.idTraccar;
this.idcliente = parada.idcliente;
this.creacion = parada.creacion;
};
Parada.create = function (req,newParada, result) {    
        sql.query("INSERT INTO parada set ?",newParada, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Parada.getById = function (req,idparada, result) {
        sql.query("SELECT  t.* FROM parada t  WHERE t.idparada= ? LIMIT 0,1", idparada, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Parada.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM parada t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Parada.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM parada t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tipo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idTraccar) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Parada.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM parada t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('parada : ', res);  

                 result(null, res);
                }
            });   
};
Parada.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM parada t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tipo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idTraccar) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('parada : ', res);  

                 result(null, res);
                }
            });   
};
Parada.updateById = function(req,idparada, parada, result){
  sql.query("UPDATE parada SET nombre = ?,tipo = ?,idTraccar = ?,idcliente = ?,creacion = ? WHERE idparada= ?",[ parada.nombre, parada.tipo, parada.idTraccar, parada.idcliente, parada.creacion,idparada], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Parada.remove = function(req,idparada, result){
     sql.query("DELETE FROM parada Where idparada=?",[idparada], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Parada;
