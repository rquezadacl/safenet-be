'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Estado object constructor
var Estado = function(req,estado){
    
this.idestado = estado.idestado;
this.nombre = estado.nombre;
};
Estado.create = function (req,newEstado, result) {    
        sql.query("INSERT INTO estado set ?",newEstado, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Estado.getById = function (req,idestado, result) {
        sql.query("SELECT  t.* FROM estado t  WHERE t.idestado= ? LIMIT 0,1", idestado, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Estado.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM estado t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Estado.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM estado t  WHERE  LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Estado.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM estado t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('estado : ', res);  

                 result(null, res);
                }
            });   
};
Estado.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM estado t  WHERE  LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('estado : ', res);  

                 result(null, res);
                }
            });   
};
Estado.updateById = function(req,idestado, estado, result){
  sql.query("UPDATE estado SET idestado = ?,nombre = ? WHERE idestado= ?",[ estado.idestado, estado.nombre,idestado], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Estado.remove = function(req,idestado, result){
     sql.query("DELETE FROM estado Where idestado=?",[idestado], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Estado;
