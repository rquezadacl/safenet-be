'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Ruta object constructor
var Ruta = function(req,ruta){
    
this.id = 0;
this.nombre = ruta.nombre;
this.origen = ruta.origen;
this.destino = ruta.destino;
this.km = ruta.km;
this.tramos = ruta.tramos;
this.paradas = ruta.paradas;
this.idcliente = ruta.idcliente;
};
Ruta.create = function (req,newRuta, result) {    
        sql.query("INSERT INTO ruta set ?",newRuta, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Ruta.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM ruta t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Ruta.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM ruta t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Ruta.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM ruta t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.km) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tramos) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.paradas) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Ruta.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM ruta t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('ruta : ', res);  

                 result(null, res);
                }
            });   
};
Ruta.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM ruta t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.km) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tramos) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.paradas) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('ruta : ', res);  

                 result(null, res);
                }
            });   
};
Ruta.updateById = function(req,id, ruta, result){
  sql.query("UPDATE ruta SET nombre = ?,origen = ?,destino = ?,km = ?,tramos = ?,paradas = ?,idcliente = ? WHERE id= ?",[ ruta.nombre, ruta.origen, ruta.destino, ruta.km, ruta.tramos, ruta.paradas, ruta.idcliente,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Ruta.remove = function(req,id, result){
     sql.query("DELETE FROM ruta Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Ruta;
