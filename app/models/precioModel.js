'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Precio object constructor
var Precio = function(req,precio){
    
this.id = 0;
this.id_origen = precio.id_origen;
this.id_destino = precio.id_destino;
this.id_cliente = precio.id_cliente;
this.costo_1 = precio.costo_1;
this.costo_2 = precio.costo_2;
this.costo_3 = precio.costo_3;
};
Precio.create = function (req,newPrecio, result) {    
        sql.query("INSERT INTO precio set ?",newPrecio, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Precio.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM precio t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Precio.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM precio t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Precio.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM precio t  WHERE  LOWER(t.id_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_3) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Precio.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM precio t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('precio : ', res);  

                 result(null, res);
                }
            });   
};
Precio.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM precio t  WHERE  LOWER(t.id_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo_3) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('precio : ', res);  

                 result(null, res);
                }
            });   
};
Precio.updateById = function(req,id, precio, result){
  sql.query("UPDATE precio SET id_origen = ?,id_destino = ?,id_cliente = ?,costo_1 = ?,costo_2 = ?,costo_3 = ? WHERE id= ?",[ precio.id_origen, precio.id_destino, precio.id_cliente, precio.costo_1, precio.costo_2, precio.costo_3,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Precio.remove = function(req,id, result){
     sql.query("DELETE FROM precio Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Precio;
