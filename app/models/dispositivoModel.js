'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Dispositivo object constructor
var Dispositivo = function(req,dispositivo){
    
this.id = 0;
this.tipo_dispositivo = dispositivo.tipo_dispositivo;
this.mac = dispositivo.mac;
this.id_cliente = dispositivo.id_cliente;
};
Dispositivo.create = function (req,newDispositivo, result) {    
        sql.query("INSERT INTO dispositivo set ?",newDispositivo, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Dispositivo.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM dispositivo t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Dispositivo.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM dispositivo t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Dispositivo.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM dispositivo t  WHERE  LOWER(t.tipo_dispositivo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.mac) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Dispositivo.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM dispositivo t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('dispositivo : ', res);  

                 result(null, res);
                }
            });   
};
Dispositivo.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM dispositivo t  WHERE  LOWER(t.tipo_dispositivo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.mac) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('dispositivo : ', res);  

                 result(null, res);
                }
            });   
};
Dispositivo.updateById = function(req,id, dispositivo, result){
  sql.query("UPDATE dispositivo SET tipo_dispositivo = ?,mac = ?,id_cliente = ? WHERE id= ?",[ dispositivo.tipo_dispositivo, dispositivo.mac, dispositivo.id_cliente,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Dispositivo.remove = function(req,id, result){
     sql.query("DELETE FROM dispositivo Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Dispositivo;
