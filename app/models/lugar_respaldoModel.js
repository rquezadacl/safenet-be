'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Lugar_Respaldo object constructor
var Lugar_Respaldo = function(req,lugar_respaldo){
    
this.id = 0;
this.nombre = lugar_respaldo.nombre;
this.codigo = lugar_respaldo.codigo;
};
Lugar_Respaldo.create = function (req,newLugar_Respaldo, result) {    
        sql.query("INSERT INTO lugar_respaldo set ?",newLugar_Respaldo, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Lugar_Respaldo.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM lugar_respaldo t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Lugar_Respaldo.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM lugar_respaldo t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Lugar_Respaldo.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM lugar_respaldo t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.codigo) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Lugar_Respaldo.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM lugar_respaldo t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('lugar_respaldo : ', res);  

                 result(null, res);
                }
            });   
};
Lugar_Respaldo.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM lugar_respaldo t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.codigo) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('lugar_respaldo : ', res);  

                 result(null, res);
                }
            });   
};
Lugar_Respaldo.updateById = function(req,id, lugar_respaldo, result){
  sql.query("UPDATE lugar_respaldo SET nombre = ?,codigo = ? WHERE id= ?",[ lugar_respaldo.nombre, lugar_respaldo.codigo,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Lugar_Respaldo.remove = function(req,id, result){
     sql.query("DELETE FROM lugar_respaldo Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Lugar_Respaldo;
