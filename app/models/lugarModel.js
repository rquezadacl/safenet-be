'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Lugar object constructor
var Lugar = function(req,lugar){
    
this.id = 0;
this.nombre = lugar.nombre;
this.codigo = lugar.codigo;
this.id_traccar = lugar.id_traccar;
};
Lugar.create = function (req,newLugar, result) {    
        sql.query("INSERT INTO lugar set ?",newLugar, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Lugar.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM lugar t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Lugar.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM lugar t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Lugar.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM lugar t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.codigo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_traccar) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Lugar.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM lugar t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('lugar : ', res);  

                 result(null, res);
                }
            });   
};
Lugar.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM lugar t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.codigo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_traccar) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('lugar : ', res);  

                 result(null, res);
                }
            });   
};
Lugar.updateById = function(req,id, lugar, result){
  sql.query("UPDATE lugar SET nombre = ?,codigo = ?,id_traccar = ? WHERE id= ?",[ lugar.nombre, lugar.codigo, lugar.id_traccar,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Lugar.remove = function(req,id, result){
     sql.query("DELETE FROM lugar Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Lugar;
