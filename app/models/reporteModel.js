'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Reporte object constructor
var Reporte = function(req,reporte){
    
this.id = 0;
this.patente = reporte.patente;
this.id_servicio = reporte.id_servicio;
this.edo_ast = reporte.edo_ast;
this.bath = reporte.bath;
this.door = reporte.door;
this.lat = reporte.lat;
this.lon = reporte.lon;
this.vel = reporte.vel;
this.dir = reporte.dir;
this.varmag = reporte.varmag;
this.fecha = reporte.fecha;
this.tramo = reporte.tramo;
this.geocercaId = reporte.geocercaId;
this.validado = reporte.validado;
this.posicionId = reporte.posicionId;
};
Reporte.create = function (req,newReporte, result) {    
        sql.query("INSERT INTO reporte set ?",newReporte, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Reporte.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM reporte t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Reporte.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM reporte t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Reporte.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM reporte t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_servicio) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.edo_ast) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.bath) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.door) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.lat) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.lon) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.vel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.dir) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.varmag) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tramo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.geocercaId) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.validado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.posicionId) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Reporte.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM reporte t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('reporte : ', res);  

                 result(null, res);
                }
            });   
};
Reporte.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM reporte t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_servicio) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.edo_ast) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.bath) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.door) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.lat) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.lon) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.vel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.dir) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.varmag) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tramo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.geocercaId) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.validado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.posicionId) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('reporte : ', res);  

                 result(null, res);
                }
            });   
};
Reporte.updateById = function(req,id, reporte, result){
  sql.query("UPDATE reporte SET patente = ?,id_servicio = ?,edo_ast = ?,bath = ?,door = ?,lat = ?,lon = ?,vel = ?,dir = ?,varmag = ?,fecha = ?,tramo = ?,geocercaId = ?,validado = ?,posicionId = ? WHERE id= ?",[ reporte.patente, reporte.id_servicio, reporte.edo_ast, reporte.bath, reporte.door, reporte.lat, reporte.lon, reporte.vel, reporte.dir, reporte.varmag, reporte.fecha, reporte.tramo, reporte.geocercaId, reporte.validado, reporte.posicionId,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Reporte.remove = function(req,id, result){
     sql.query("DELETE FROM reporte Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Reporte;
