'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Gps_Data_Devices object constructor
var Gps_Data_Devices = function(req,gps_data_devices){
    
this.id = 0;
this.imei = gps_data_devices.imei;
this.longitude = gps_data_devices.longitude;
this.latitude = gps_data_devices.latitude;
this.angle = gps_data_devices.angle;
this.altitude = gps_data_devices.altitude;
this.satellites = gps_data_devices.satellites;
this.speed = gps_data_devices.speed;
this.datetime = gps_data_devices.datetime;
};
Gps_Data_Devices.create = function (req,newGps_Data_Devices, result) {    
        sql.query("INSERT INTO gps_data_devices set ?",newGps_Data_Devices, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Gps_Data_Devices.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM gps_data_devices t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Gps_Data_Devices.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM gps_data_devices t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Gps_Data_Devices.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM gps_data_devices t  WHERE  LOWER(t.imei) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.longitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.latitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.angle) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.altitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.satellites) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.speed) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.datetime) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Gps_Data_Devices.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM gps_data_devices t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('gps_data_devices : ', res);  

                 result(null, res);
                }
            });   
};
Gps_Data_Devices.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM gps_data_devices t  WHERE  LOWER(t.imei) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.longitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.latitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.angle) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.altitude) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.satellites) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.speed) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.datetime) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('gps_data_devices : ', res);  

                 result(null, res);
                }
            });   
};
Gps_Data_Devices.updateById = function(req,id, gps_data_devices, result){
  sql.query("UPDATE gps_data_devices SET imei = ?,longitude = ?,latitude = ?,angle = ?,altitude = ?,satellites = ?,speed = ?,datetime = ? WHERE id= ?",[ gps_data_devices.imei, gps_data_devices.longitude, gps_data_devices.latitude, gps_data_devices.angle, gps_data_devices.altitude, gps_data_devices.satellites, gps_data_devices.speed, gps_data_devices.datetime,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Gps_Data_Devices.remove = function(req,id, result){
     sql.query("DELETE FROM gps_data_devices Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Gps_Data_Devices;
