'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Bus object constructor
var Bus = function(req,bus){
    
this.id = 0;
this.id_cliente = bus.id_cliente;
this.patente = bus.patente;
this.imei = bus.imei;
this.marca = bus.marca;
this.modelo = bus.modelo;
this.status = bus.status;
this.cap_piso1 = bus.cap_piso1;
this.cap_piso2 = bus.cap_piso2;
this.clase_piso1 = bus.clase_piso1;
this.clase_piso2 = bus.clase_piso2;
this.mantenimiento = bus.mantenimiento;
this.bath = bus.bath;
this.creacion = bus.creacion;
this.chasis = bus.chasis;
this.numero_maquina = bus.numero_maquina;
this.conductor = bus.conductor;
this.motor = bus.motor;
this.ano = bus.ano;
};
Bus.create = function (req,newBus, result) {    
        sql.query("INSERT INTO bus set ?",newBus, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Bus.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM bus t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Bus.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM bus t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Bus.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM bus t  WHERE  LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.imei) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.marca) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.modelo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.cap_piso1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.cap_piso2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase_piso1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase_piso2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.mantenimiento) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.bath) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.chasis) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.numero_maquina) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.conductor) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.motor) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.ano) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Bus.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM bus t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('bus : ', res);  

                 result(null, res);
                }
            });   
};
Bus.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM bus t  WHERE  LOWER(t.id_cliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.imei) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.marca) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.modelo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.cap_piso1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.cap_piso2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase_piso1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase_piso2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.mantenimiento) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.bath) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.chasis) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.numero_maquina) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.conductor) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.motor) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.ano) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('bus : ', res);  

                 result(null, res);
                }
            });   
};
Bus.updateById = function(req,id, bus, result){
  sql.query("UPDATE bus SET id_cliente = ?,patente = ?,imei = ?,marca = ?,modelo = ?,status = ?,cap_piso1 = ?,cap_piso2 = ?,clase_piso1 = ?,clase_piso2 = ?,mantenimiento = ?,bath = ?,creacion = ?,chasis = ?,numero_maquina = ?,conductor = ?,motor = ?,ano = ? WHERE id= ?",[ bus.id_cliente, bus.patente, bus.imei, bus.marca, bus.modelo, bus.status, bus.cap_piso1, bus.cap_piso2, bus.clase_piso1, bus.clase_piso2, bus.mantenimiento, bus.bath, bus.creacion, bus.chasis, bus.numero_maquina, bus.conductor, bus.motor, bus.ano,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Bus.remove = function(req,id, result){
     sql.query("DELETE FROM bus Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Bus;
