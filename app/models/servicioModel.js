'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Servicio object constructor
var Servicio = function(req,servicio){
    
this.id = 0;
this.patente = servicio.patente;
this.id_caja = servicio.id_caja;
this.origen = servicio.origen;
this.destino = servicio.destino;
this.ruta = servicio.ruta;
this.fecha = servicio.fecha;
this.hora = servicio.hora;
this.status = servicio.status;
this.caja = servicio.caja;
this.fechafin = servicio.fechafin;
this.horafin = servicio.horafin;
this.idcliente = servicio.idcliente;
this.nombre = servicio.nombre;
};
Servicio.create = function (req,newServicio, result) {    
        sql.query("INSERT INTO servicio set ?",newServicio, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Servicio.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM servicio t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Servicio.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM servicio t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Servicio.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM servicio t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_caja) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.ruta) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.hora) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.caja) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fechafin) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.horafin) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Servicio.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM servicio t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('servicio : ', res);  

                 result(null, res);
                }
            });   
};
Servicio.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM servicio t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_caja) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.ruta) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.hora) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.caja) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fechafin) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.horafin) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('servicio : ', res);  

                 result(null, res);
                }
            });   
};
Servicio.updateById = function(req,id, servicio, result){
  sql.query("UPDATE servicio SET patente = ?,id_caja = ?,origen = ?,destino = ?,ruta = ?,fecha = ?,hora = ?,status = ?,caja = ?,fechafin = ?,horafin = ?,idcliente = ?,nombre = ? WHERE id= ?",[ servicio.patente, servicio.id_caja, servicio.origen, servicio.destino, servicio.ruta, servicio.fecha, servicio.hora, servicio.status, servicio.caja, servicio.fechafin, servicio.horafin, servicio.idcliente, servicio.nombre,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Servicio.remove = function(req,id, result){
     sql.query("DELETE FROM servicio Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Servicio;
