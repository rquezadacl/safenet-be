'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Cliente object constructor
var Cliente = function(req,cliente){
    
this.id = 0;
this.rut = cliente.rut;
this.nombre = cliente.nombre;
this.terminal_origen = cliente.terminal_origen;
this.direccion = cliente.direccion;
this.creacion = cliente.creacion;
this.idestado = cliente.idestado;
this.servicios = cliente.servicios;
this.subservicios = cliente.subservicios;
};
Cliente.create = function (req,newCliente, result) {    
        sql.query("INSERT INTO cliente set ?",newCliente, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Cliente.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM cliente t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Cliente.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM cliente t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Cliente.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM cliente t  WHERE  LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.terminal_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.direccion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.servicios) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.subservicios) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Cliente.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM cliente t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('cliente : ', res);  

                 result(null, res);
                }
            });   
};
Cliente.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM cliente t  WHERE  LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.terminal_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.direccion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.servicios) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.subservicios) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('cliente : ', res);  

                 result(null, res);
                }
            });   
};
Cliente.updateById = function(req,id, cliente, result){
  sql.query("UPDATE cliente SET rut = ?,nombre = ?,terminal_origen = ?,direccion = ?,creacion = ?,idestado = ?,servicios = ?,subservicios = ? WHERE id= ?",[ cliente.rut, cliente.nombre, cliente.terminal_origen, cliente.direccion, cliente.creacion, cliente.idestado, cliente.servicios, cliente.subservicios,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Cliente.remove = function(req,id, result){
     sql.query("DELETE FROM cliente Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Cliente;
