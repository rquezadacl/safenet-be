'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Tramo object constructor
var Tramo = function(req,tramo){
    
this.id = 0;
this.nombre = tramo.nombre;
this.origen = tramo.origen;
this.destino = tramo.destino;
this.km = tramo.km;
this.costo1 = tramo.costo1;
this.costo2 = tramo.costo2;
this.costo3 = tramo.costo3;
this.costo4 = tramo.costo4;
this.idTraccar = tramo.idTraccar;
this.creacion = tramo.creacion;
this.status = tramo.status;
this.idcliente = tramo.idcliente;
};
Tramo.create = function (req,newTramo, result) {    
        sql.query("INSERT INTO tramo set ?",newTramo, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Tramo.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM tramo t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Tramo.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM tramo t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Tramo.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM tramo t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.km) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo3) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo4) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idTraccar) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Tramo.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM tramo t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tramo : ', res);  

                 result(null, res);
                }
            });   
};
Tramo.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM tramo t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.km) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo1) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo2) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo3) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.costo4) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idTraccar) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tramo : ', res);  

                 result(null, res);
                }
            });   
};
Tramo.updateById = function(req,id, tramo, result){
  sql.query("UPDATE tramo SET nombre = ?,origen = ?,destino = ?,km = ?,costo1 = ?,costo2 = ?,costo3 = ?,costo4 = ?,idTraccar = ?,creacion = ?,status = ?,idcliente = ? WHERE id= ?",[ tramo.nombre, tramo.origen, tramo.destino, tramo.km, tramo.costo1, tramo.costo2, tramo.costo3, tramo.costo4, tramo.idTraccar, tramo.creacion, tramo.status, tramo.idcliente,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Tramo.remove = function(req,id, result){
     sql.query("DELETE FROM tramo Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Tramo;
