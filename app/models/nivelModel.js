'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Nivel object constructor
var Nivel = function(req,nivel){
    
this.idnivel = nivel.idnivel;
this.nombre = nivel.nombre;
};
Nivel.create = function (req,newNivel, result) {    
        sql.query("INSERT INTO nivel set ?",newNivel, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Nivel.getById = function (req,idnivel, result) {
        sql.query("SELECT  t.* FROM nivel t  WHERE t.idnivel= ? LIMIT 0,1", idnivel, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Nivel.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM nivel t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Nivel.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM nivel t  WHERE  LOWER(t.idnivel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Nivel.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM nivel t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('nivel : ', res);  

                 result(null, res);
                }
            });   
};
Nivel.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM nivel t  WHERE  LOWER(t.idnivel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('nivel : ', res);  

                 result(null, res);
                }
            });   
};
Nivel.updateById = function(req,idnivel, nivel, result){
  sql.query("UPDATE nivel SET idnivel = ?,nombre = ? WHERE idnivel= ?",[ nivel.idnivel, nivel.nombre,idnivel], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Nivel.remove = function(req,idnivel, result){
     sql.query("DELETE FROM nivel Where idnivel=?",[idnivel], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Nivel;
