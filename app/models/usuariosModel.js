'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Usuarios object constructor
var Usuarios = function(req,usuarios){
    
this.idusuario = 0;
this.nombre = usuarios.nombre;
this.apellido = usuarios.apellido;
this.email = usuarios.email;
this.username = usuarios.username;
this.password = usuarios.password;
this.idrol = usuarios.idrol;
this.creacion = usuarios.creacion;
this.idestado = usuarios.idestado;
this.activado = usuarios.activado;
this.rut = usuarios.rut;
this.idcliente = usuarios.idcliente;
};
Usuarios.login = function(username,result){
    sql.query("SELECT u.idusuario, u.nombre, u.apellido, u.email, u.username, u.password, u.idrol, u.idestado, u.activado, u.idcliente  FROM safe_net.usuarios u where u.username = ? LIMIT 0,1",username, function (err, res) {
                
        if(err) {
            console.log("error: ", err);
            result(err, null);
        }
        else if(res && res.length>0){
            result(null, res);
      
        }else{
            result("Record Not Found", null);
        }
    });           
};
Usuarios.create = function (req,newUsuarios, result) {    
        sql.query("INSERT INTO usuarios set ?",newUsuarios, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Usuarios.getById = function (req,idusuario, result) {
        sql.query("SELECT  t.* FROM usuarios t  WHERE t.idusuario= ? LIMIT 0,1", idusuario, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Usuarios.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM usuarios t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Usuarios.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM usuarios t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.apellido) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.email) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.username) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.password) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idrol) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.activado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Usuarios.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM usuarios t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('usuarios : ', res);  

                 result(null, res);
                }
            });   
};
Usuarios.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM usuarios t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.apellido) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.email) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.username) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.password) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idrol) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.activado) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idcliente) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('usuarios : ', res);  

                 result(null, res);
                }
            });   
};
Usuarios.updateById = function(req,idusuario, usuarios, result){
  sql.query("UPDATE usuarios SET nombre = ?,apellido = ?,email = ?,username = ?,password = ?,idrol = ?,creacion = ?,idestado = ?,activado = ?,rut = ?,idcliente = ? WHERE idusuario= ?",[ usuarios.nombre, usuarios.apellido, usuarios.email, usuarios.username, usuarios.password, usuarios.idrol, usuarios.creacion, usuarios.idestado, usuarios.activado, usuarios.rut, usuarios.idcliente,idusuario], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Usuarios.remove = function(req,idusuario, result){
     sql.query("DELETE FROM usuarios Where idusuario=?",[idusuario], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Usuarios;
