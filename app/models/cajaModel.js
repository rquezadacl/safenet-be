'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Caja object constructor
var Caja = function(req,caja){
    
this.id = 0;
this.patente = caja.patente;
this.fecha_aper = caja.fecha_aper;
this.fecha_cierre = caja.fecha_cierre;
this.status = caja.status;
this.monto = caja.monto;
};
Caja.create = function (req,newCaja, result) {    
        sql.query("INSERT INTO caja set ?",newCaja, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Caja.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM caja t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Caja.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM caja t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Caja.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM caja t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha_aper) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha_cierre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.monto) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Caja.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM caja t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('caja : ', res);  

                 result(null, res);
                }
            });   
};
Caja.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM caja t  WHERE  LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha_aper) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fecha_cierre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.status) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.monto) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('caja : ', res);  

                 result(null, res);
                }
            });   
};
Caja.updateById = function(req,id, caja, result){
  sql.query("UPDATE caja SET patente = ?,fecha_aper = ?,fecha_cierre = ?,status = ?,monto = ? WHERE id= ?",[ caja.patente, caja.fecha_aper, caja.fecha_cierre, caja.status, caja.monto,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Caja.remove = function(req,id, result){
     sql.query("DELETE FROM caja Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Caja;
