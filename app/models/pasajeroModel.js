'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Pasajero object constructor
var Pasajero = function(req,pasajero){
    
this.id = 0;
this.rut = pasajero.rut;
this.nombre = pasajero.nombre;
this.fono = pasajero.fono;
this.nro_asiento = pasajero.nro_asiento;
this.nro_pasaje = pasajero.nro_pasaje;
this.id_servicio = pasajero.id_servicio;
this.tipovta = pasajero.tipovta;
this.formapago = pasajero.formapago;
this.monto = pasajero.monto;
this.id_tramo = pasajero.id_tramo;
this.id_origen = pasajero.id_origen;
this.id_destino = pasajero.id_destino;
this.patente = pasajero.patente;
this.activo = pasajero.activo;
};
Pasajero.create = function (req,newPasajero, result) {    
        sql.query("INSERT INTO pasajero set ?",newPasajero, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Pasajero.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM pasajero t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Pasajero.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM pasajero t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Pasajero.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM pasajero t  WHERE  LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fono) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nro_asiento) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nro_pasaje) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_servicio) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tipovta) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.formapago) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.monto) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_tramo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.activo) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Pasajero.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM pasajero t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('pasajero : ', res);  

                 result(null, res);
                }
            });   
};
Pasajero.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM pasajero t  WHERE  LOWER(t.rut) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.fono) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nro_asiento) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nro_pasaje) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_servicio) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.tipovta) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.formapago) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.monto) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_tramo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_origen) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.id_destino) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.patente) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.activo) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('pasajero : ', res);  

                 result(null, res);
                }
            });   
};
Pasajero.updateById = function(req,id, pasajero, result){
  sql.query("UPDATE pasajero SET rut = ?,nombre = ?,fono = ?,nro_asiento = ?,nro_pasaje = ?,id_servicio = ?,tipovta = ?,formapago = ?,monto = ?,id_tramo = ?,id_origen = ?,id_destino = ?,patente = ?,activo = ? WHERE id= ?",[ pasajero.rut, pasajero.nombre, pasajero.fono, pasajero.nro_asiento, pasajero.nro_pasaje, pasajero.id_servicio, pasajero.tipovta, pasajero.formapago, pasajero.monto, pasajero.id_tramo, pasajero.id_origen, pasajero.id_destino, pasajero.patente, pasajero.activo,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Pasajero.remove = function(req,id, result){
     sql.query("DELETE FROM pasajero Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Pasajero;
