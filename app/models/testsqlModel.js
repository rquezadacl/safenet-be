'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Testsql object constructor
var Testsql = function(req,testsql){
    
this.id = 0;
this.raw = testsql.raw;
this.creacion = testsql.creacion;
};
Testsql.create = function (req,newTestsql, result) {    
        sql.query("INSERT INTO testsql set ?",newTestsql, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Testsql.getById = function (req,id, result) {
        sql.query("SELECT  t.* FROM testsql t  WHERE t.id= ? LIMIT 0,1", id, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Testsql.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM testsql t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Testsql.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM testsql t  WHERE  LOWER(t.raw) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Testsql.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM testsql t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('testsql : ', res);  

                 result(null, res);
                }
            });   
};
Testsql.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM testsql t  WHERE  LOWER(t.raw) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('testsql : ', res);  

                 result(null, res);
                }
            });   
};
Testsql.updateById = function(req,id, testsql, result){
  sql.query("UPDATE testsql SET raw = ?,creacion = ? WHERE id= ?",[ testsql.raw, testsql.creacion,id], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Testsql.remove = function(req,id, result){
     sql.query("DELETE FROM testsql Where id=?",[id], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Testsql;
