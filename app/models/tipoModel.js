'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Tipo object constructor
var Tipo = function(req,tipo){
    
this.idtipo = tipo.idtipo;
this.nombre = tipo.nombre;
};
Tipo.create = function (req,newTipo, result) {    
        sql.query("INSERT INTO tipo set ?",newTipo, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Tipo.getById = function (req,idtipo, result) {
        sql.query("SELECT  t.* FROM tipo t  WHERE t.idtipo= ? LIMIT 0,1", idtipo, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Tipo.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM tipo t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Tipo.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM tipo t  WHERE  LOWER(t.idtipo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Tipo.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM tipo t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tipo : ', res);  

                 result(null, res);
                }
            });   
};
Tipo.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM tipo t  WHERE  LOWER(t.idtipo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('tipo : ', res);  

                 result(null, res);
                }
            });   
};
Tipo.updateById = function(req,idtipo, tipo, result){
  sql.query("UPDATE tipo SET idtipo = ?,nombre = ? WHERE idtipo= ?",[ tipo.idtipo, tipo.nombre,idtipo], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Tipo.remove = function(req,idtipo, result){
     sql.query("DELETE FROM tipo Where idtipo=?",[idtipo], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Tipo;
