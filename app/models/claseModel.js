'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Clase object constructor
var Clase = function(req,clase){
    
this.idclase = 0;
this.nombre = clase.nombre;
};
Clase.create = function (req,newClase, result) {    
        sql.query("INSERT INTO clase set ?",newClase, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Clase.getById = function (req,idclase, result) {
        sql.query("SELECT  t.* FROM clase t  WHERE t.idclase= ? LIMIT 0,1", idclase, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Clase.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM clase t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Clase.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM clase t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Clase.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM clase t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('clase : ', res);  

                 result(null, res);
                }
            });   
};
Clase.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM clase t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('clase : ', res);  

                 result(null, res);
                }
            });   
};
Clase.updateById = function(req,idclase, clase, result){
  sql.query("UPDATE clase SET nombre = ? WHERE idclase= ?",[ clase.nombre,idclase], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Clase.remove = function(req,idclase, result){
     sql.query("DELETE FROM clase Where idclase=?",[idclase], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Clase;
