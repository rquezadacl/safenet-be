'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Roles object constructor
var Roles = function(req,roles){
    
this.idrol = 0;
this.nombre = roles.nombre;
this.comentario = roles.comentario;
};
Roles.create = function (req,newRoles, result) {    
        sql.query("INSERT INTO roles set ?",newRoles, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Roles.getById = function (req,idrol, result) {
        sql.query("SELECT  t.* FROM roles t  WHERE t.idrol= ? LIMIT 0,1", idrol, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Roles.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM roles t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Roles.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM roles t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.comentario) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Roles.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM roles t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('roles : ', res);  

                 result(null, res);
                }
            });   
};
Roles.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM roles t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.comentario) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('roles : ', res);  

                 result(null, res);
                }
            });   
};
Roles.updateById = function(req,idrol, roles, result){
  sql.query("UPDATE roles SET nombre = ?,comentario = ? WHERE idrol= ?",[ roles.nombre, roles.comentario,idrol], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Roles.remove = function(req,idrol, result){
     sql.query("DELETE FROM roles Where idrol=?",[idrol], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Roles;
