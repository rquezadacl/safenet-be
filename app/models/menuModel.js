'user strict';
var sql = require('./newdb.js');
var url = require('url');
var helper = require('./helper');
//Menu object constructor
var Menu = function(req,menu){
    
this.idmenu = 0;
this.nombre = menu.nombre;
this.nivel = menu.nivel;
this.creacion = menu.creacion;
this.sistema = menu.sistema;
this.modulo = menu.modulo;
this.padre = menu.padre;
this.orden = menu.orden;
this.clase = menu.clase;
this.idestado = menu.idestado;
};
Menu.create = function (req,newMenu, result) {    
        sql.query("INSERT INTO menu set ?",newMenu, function (err, res) {
                
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    console.log(res.insertId);
                    result(null, res.insertId);
                }
            });           
};
Menu.getById = function (req,idmenu, result) {
        sql.query("SELECT  t.* FROM menu t  WHERE t.idmenu= ? LIMIT 0,1", idmenu, function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else if(res && res.length>0){
                    result(null, res);
              
                }else{
                    result("Record Not Found", null);
                }
            });   
};
Menu.totalCount = function (req,result) {
        sql.query("SELECT count(*) TotalCount FROM menu t  ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Menu.totalSearchCount = function (req,searchKey,result) {
        sql.query("SELECT count(*) TotalCount FROM menu t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nivel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.sistema) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.modulo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.padre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.orden) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') ", function (err, res) {             
                if(err) {
                    console.log("error: ", err);
                    result(err, null);
                }
                else{
                    result(null, res);
              
                }
            });   
};

Menu.getAll = function (req,offset,pageSize,result) {
        sql.query("SELECT  t.* FROM menu t  LIMIT ?, ?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('menu : ', res);  

                 result(null, res);
                }
            });   
};
Menu.search = function (req,searchKey,offset,pageSize,result) {

        sql.query("SELECT  t.* FROM menu t  WHERE  LOWER(t.nombre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.nivel) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.creacion) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.sistema) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.modulo) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.padre) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.orden) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.clase) LIKE CONCAT('%','"+searchKey+"','%') OR LOWER(t.idestado) LIKE CONCAT('%','"+searchKey+"','%') LIMIT ?,?",[offset,pageSize],function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
                  console.log('menu : ', res);  

                 result(null, res);
                }
            });   
};
Menu.updateById = function(req,idmenu, menu, result){
  sql.query("UPDATE menu SET nombre = ?,nivel = ?,creacion = ?,sistema = ?,modulo = ?,padre = ?,orden = ?,clase = ?,idestado = ? WHERE idmenu= ?",[ menu.nombre, menu.nivel, menu.creacion, menu.sistema, menu.modulo, menu.padre, menu.orden, menu.clase, menu.idestado,idmenu], function (err, res) {
          if(err) {
              console.log("error: ", err);
                result(null, err);
             }
           else{   
             result(null, res);
                }
            }); 
};
Menu.remove = function(req,idmenu, result){
     sql.query("DELETE FROM menu Where idmenu=?",[idmenu], function (err, res) {

                if(err) {
                    console.log("error: ", err);
                    result(null, err);
                }
                else{
               
                 result(null, res);
                }
            }); 
};

module.exports= Menu;
